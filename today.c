/*
Today.c έκδοση 0.66
26 Ιουνίου 2010
A program that displays fixed and movable feasts and birthdays
Linux version by Christos Nouskas (nous at archlinux.us)
Many fixes by Vivia Nikolaidou (vivia at ee.auth.gr)

This utility is released under the GNU General Public License. Feel free to modify.
Share and enjoy!
*/

#define GLOBAL "/usr/local/share/today/today.dat"
#define BEFORE 0
#define AFTER 2
#define LINELEN 40
#define MAXLEN 200
/* #define USAGE " [-t|-x|-g] [Terminal(default)|XOSD|GTK]\n" */
#define ERRNODAT "Δεν βρήκα τα αρχεία .dat..."

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <unistd.h>
/*
#include <iconv.h>
#include <langinfo.h>
*/

static char line[MAXLEN], koko[]="00/00", matches[30][MAXLEN], offset[4];
static int kokolen, ct=0, offseti, rtoday;
static int KRA_KRA_EKANE_TO_MAYRO_KORAKI=0;

int main(int argc, char **argv) 
{
    char *convert(char *, size_t );
    int i=0, k=0;
    char *errmsg;
    void parse(FILE *);
    FILE *global, *personal=NULL, *config;
    struct tm *d;
    static int months[]={0,31,28,31,30,31,30,31,31,30,31,30,31};
    int before=BEFORE, after=AFTER;
    char *BE="before=", *AF="after=", *home, *conf, *temp, intro[MAXLEN];
    int day, month, yester, Y, leap,/* K, I1, I2, L, M, D,*/ easter, p;
    time_t t=time(NULL);
    d=localtime(&t);
    errmsg=(char *) malloc(100*sizeof(char));

/*    if (argc>2 || (argc==2 && strcmp(argv[1], "-x") && strcmp(argv[1], "-c"))) {
        printf("%s: %s", argv[0], USAGE);
	exit(1);
    } */

/*    if ((lcctype=(char *)getenv("LC_CTYPE"))!=NULL) {
	if (!strstr(lcctype, "UTF") || !strstr(lcctype, "utf")) {
	    UTF=0;
	}
    }
    else if ((lcctype=(char *)getenv("LC_ALL"))!=NULL) {
    	if (!strstr(lcctype, "UTF") || !strstr(lcctype, "utf")) {
    	    UTF=0;
    	}
    } */

    if ((temp=getenv("HOME"))!=NULL) {
	conf=(char *)malloc((strlen(temp)+16)*sizeof(char *));
	strncpy(conf, temp, strlen(temp)+1);
	conf=strcat(conf, "/.today/config");
	if ((config=fopen(conf, "r"))!=NULL) {
	    while (fgets(line, MAXLEN, config) != NULL) {
		if (!strncmp(line, BE, strlen(BE))) {
		    before=atoi(line+strlen(BE));
		}
		if (!strncmp(line, AF, strlen(AF))) {
        	    after=atoi(line+strlen(AF));
        	}
	    }
	fclose(config);
	}
	home=(char *)malloc((strlen(temp)+22)*sizeof(char *));
	strncpy(home,temp,strlen(temp)+1);
	home=strcat(home, "/.today/personal.dat");
	if ((personal=fopen(home, "r"))==NULL) {
    	    ;
    	}
    }

    if ((global=fopen(GLOBAL, "r"))==NULL) {
	errmsg=ERRNODAT;
	puts(errmsg);
	exit(1);
    }

    Y=(d->tm_year)+1900;
    leap=((Y%400==0)||((Y%100)&&(Y%4==0)));

/* Old way*/
/*
    months[2]+=leap;
    K=Y%19;
    I1=((19*K)+15)%30;
    I2=(Y+(int)(Y/4)+I1)%7;
    L=I1-I2;
    M=3+(int)((L+40)/44);
    D=L+28-(int)((31*M/4));
*/
/* D seems to need corrections ;-) */
/*
    D=(D>17 && M==4) ? (M=5, D=D-17) : ( (D<18 && M==4) ? D=D+13 : (M=4, D=D+5));
    easter=31+28+leap+31+(M==5?30:0)+D;
*/
    p=13+((19*(Y%19)+15)%30)+(2*(Y%4)+4*(Y%7)+6*(((19*(Y%19)+15)%30)+1))%7;
    easter=31+28+leap+21+p;
    rtoday=d->tm_yday-easter;
    day=d->tm_mday;
    month=d->tm_mon+1;

    /* Start printing names */
    if (before) {
	if (day==1&&month==1) {
	    sprintf(koko,"%d/%d",31,12);
	    month=12;
	    day=31;
	}
	else {
	    sprintf(koko,"%d/%d", yester=(day==1?months[--month]:day-1), (day==1?month-1:month));
	}
	kokolen=strlen(koko);
	rtoday--;
	parse(personal);
	parse(global);
	sprintf(intro, "Χθές %s %s", koko, ct==0?"δεν γιόρταζε κανείς.":(ct>1?"γιόρταζαν:":"γιόρταζε:"));
	printf("%s\n", intro);
	for (i=0; i<ct; i++) {
	    printf("  %s\n", matches[i]);
	}
    }

    day=d->tm_mday;
    month=d->tm_mon+1;
    rtoday=d->tm_yday-easter;
    sprintf(koko,"%d/%d", day, month);
    kokolen=strlen(koko);
    KRA_KRA_EKANE_TO_MAYRO_KORAKI=1;
    parse(personal);
    parse(global);
    sprintf(intro, "Σήμερα %s %s", koko, ct==0?"δεν γιορτάζει κανείς.":(ct>1?"γιορτάζουν:":"γιορτάζει:"));
    printf("%s\n", intro);

    for (i=0; i<ct; i++) {
	printf("  %s\n", matches[i]);
    }

    if (after) {
	while (k++<after) {
	    if (++day>months[month]) {
		day=1;
		month=month==12?1:++month;
	    }
	    sprintf(koko,"%d/%d", day, month);
	    kokolen=strlen(koko);
	    rtoday++;
	    KRA_KRA_EKANE_TO_MAYRO_KORAKI=1;
	    parse(personal);
	    parse(global);
	    sprintf(intro, "%s %s %s",  k==1?"Αύριο":(k==2?"Μεθαύριο":"Στις"), koko,\
		    ct==0?"δεν γιορτάζει κανείς.":(ct>1?"γιορτάζουν:":"γιορτάζει:"));
	    printf("%s\n", intro);
	    for (i=0; i<ct; i++) {
		printf("  %s\n", matches[i]);
	    }
	}
    }
    if (personal) {
	fclose(personal);
    }
    if (global) {
	fclose(global);
    }
    return 0;

}

void parse(FILE *f)
{
/*    int isblank(int); */
    int i=0;
    if (!f) {
	return;
    }
    rewind(f);
    if (KRA_KRA_EKANE_TO_MAYRO_KORAKI) {
	memset(matches, (int) 0, sizeof(matches));
	ct=0;
	KRA_KRA_EKANE_TO_MAYRO_KORAKI=0;
    }
    while (fgets(line, MAXLEN, f) !=NULL ) {
	if (line[0]!=';' && line[0]!='#') {
	    /* check for relative-to-Easter dates first */
	    if (line[0]=='+' || line[0]=='-') {
		i=1;
		while ((isdigit(line[i])) && (i<4))
		    strncpy(offset+1, line+1, i++);
		offset[0]=(line[0]=='-')?'-':' ';
		if ((offseti=atoi(offset))==rtoday) {
		    strncpy(matches[ct++], line+strlen(offset)+1, strlen(line)-strlen(offset)-2);
		}
		memset(offset, (int) 0, 4);
	    }
	    /* absolute dates */
    	    if (isblank(line[kokolen])) {	/* skip 2-digit month ambiguity */
		if (!strncmp(line, koko, kokolen)) {
		    strncpy(matches[ct++], line+kokolen+1, strlen(line)-kokolen-2);
		}
	    }
	}
    }
}
