# Today.c
# A small utility that displays fixed and movable feasts and birthdays
# First Linux/UNIX  version June 2002
# by Christos Nouskas (nous at archlinux.us)
# Inspired by the weeniedows version made by Haris Kakoulides (www.harka.com)
# This utility is released under the GNU General Public License.

# Share and enjoy!

CC=gcc

ifndef CFLAGS
    CFLAGS=-O2 -ansi -pedantic
endif

# where to install
PREFIX=/usr/local

today: today.c
	$(CC) -o today $(CFLAGS) today.c

strip:
	strip today

clean:
	rm -f today

install: today
	strip today
	cp today $(PREFIX)/bin
	chmod 755 $(PREFIX)/bin/today
	mkdir -p $(PREFIX)/share/today
	mkdir -p $(HOME)/.today
	[ -f $(PREFIX)/share/today/today.dat ] || cp today.dat $(PREFIX)/share/today/today.dat
	[ -f $(HOME)/.today/config ] || cp config $(HOME)/.today/config
	[ -f $(HOME)/.today/personal.dat ] || cp personal.dat $(HOME)/.today/personal.dat
	chown -R `basename $(HOME)` $(HOME)/.today 

uninstall:
	rm -f $(PREFIX)/bin/today
	rm -fr $(PREFIX)/share/today
	rm -fr $(HOME)/.today
